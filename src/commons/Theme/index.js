import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    color: {
        primary: "#D32F2F",
        secondary: "#448AFF",
        error: "#455A64",
    },
    typography: {
        fontFamily: "Roboto",
    },
});

export default theme;
