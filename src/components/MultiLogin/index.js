import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { Radio, RadioGroup } from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import styles from "./style.js";

function MultiLogin({
  Login = "",
  Register = "",
  ForgotPass = "",
  classes,
  handleEvent,
}) {
  const [formName, setFormName] = useState("");
  const [value, setValue] = useState({});

  useEffect(() => {
    ValidatorForm.addValidationRule("checkPass", (pass) => {
      ValidatorForm.addValidationRule("isPasswordMatch", (cfPass) => {
        if (cfPass !== value.pass) {
          return false;
        }
        return true;
      });

      let len = pass.length;
      if (len > 0 && len < 8) {
        return false;
      }
      return true;
    });
  }, [value.pass]);

  useEffect(() => {
    ValidatorForm.addValidationRule("isPasswordMatch", (cfPass) => {
      if (value.cfPass !== value.pass) {
        return false;
      }
      return true;
    });
  }, [value.cfPass]);

  useEffect(() => {
    setFormName(Login + Register + ForgotPass);
    setValue({ email: "", pass: "", gender: "" });
  }, [Login, Register, ForgotPass]);

  function handleSubmit(e) {
    e.preventDefault();
    handleEvent(value);
  }


  return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <AccountCircleIcon fontSize={"large"} />
          </Avatar>
          <Typography component="h1" variant="h5">
            {formName === "login"
              ? "Log In"
              : formName === "register"
              ? "Register"
              : "Forgot password"}
          </Typography>
          <ValidatorForm onSubmit={handleSubmit} className={classes.form}>
            <TextValidator
              variant="outlined"
              margin="normal"
              name="email"
              fullWidth
              id="email"
              label="Email Address"
              autoComplete="email"
              autoFocus
              value={value?.email || ""}
              onChange={(e) => setValue({ ...value, email: e.target.value })}
              required
              validators={["required", "isEmail"]}
              errorMessages={["This field is required", "Email is not valid"]}
            />

            {(formName === "login" || formName === "register") && (
              <TextValidator
                variant="outlined"
                margin="normal"
                required
                fullWidth
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={value?.pass || ""}
                onChange={(e) => setValue({ ...value, pass: e.target.value })}
                validators={["checkPass", "required"]}
                errorMessages={[
                  "Password minimum of 8 characters!",
                  "This field is required",
                ]}
              />
            )}

            {formName === "register" && (
              <>
                <TextValidator
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="Confirm password"
                  type="password"
                  id="Confirmpassword"
                  autoComplete="current-password"
                  value={value?.cfPass || ""}
                  onChange={(e) =>
                    setValue({ ...value, cfPass: e.target.value })
                  }
                  validators={["isPasswordMatch", "required"]}
                  errorMessages={[
                    "Password not match!",
                    "This field is required",
                  ]}
                />
                <RadioGroup
                  aria-label="gender"
                  value={value?.gender || setValue({...value,gender:"male"})}
                  onChange={(e) =>
                    setValue({ ...value, gender: e.target.value })
                  }
                  className={classes.radioGroup}
                >
                  <FormControlLabel
                    value="male"
                    control={<Radio />}
                    label="Male"
                  />
                  <FormControlLabel
                    value="female"
                    control={<Radio />}
                    label="Female"
                  />
                </RadioGroup>
              </>
            )}
            {formName === "login" && (
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
            )}

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              {formName === "login"
                ? " Sign In"
                : formName === "register"
                ? "Register"
                : "Submit"}
            </Button>
            <Grid container>
              <Grid item xs>
                {formName !== "login" && formName !== "register" ? (
                  <Link to="/" variant="body2">
                    Log In
                  </Link>
                ) : (
                  <Link to="/ForgotPass" variant="body2">
                    Forgot password?
                  </Link>
                )}
              </Grid>
              <Grid item>
                {formName === "register" ? (
                  <Link to="/" variant="body2">
                    Log In
                  </Link>
                ) : (
                  <Link to="/Register" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                )}
              </Grid>
            </Grid>
          </ValidatorForm>
        </div>
        <Box mt={8}></Box>
      </Container>
  );
}

export default withStyles(styles)(MultiLogin);
