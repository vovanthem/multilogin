
const styles = theme => ({
    paper: {
      marginTop: "40px",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    avatar: {
      margin: "8px",
      backgroundColor: theme.color.primary,
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: "8px",
    },
    submit: {
      marginTop: "24px",
      marginBottom: "16px"
    },
    radioGroup: {
      flexDirection: "row",
    },
  });
export default styles;