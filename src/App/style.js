const styles = () => ({
    menuList: {
        display: 'flex',
      },
      item: {
        listStyleType: 'none',
        paddingRight:'10px',
      }
});

export default styles;