import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,

} from "react-router-dom";
import MultiLogin from "../components/MultiLogin";
import {ThemeProvider} from '@material-ui/styles';
import theme from '../commons/Theme';
import { withStyles } from "@material-ui/core";
import styles from './style.js';


function App(props) {
  
  const {classes} = props;

  function handleLogin(value) {
    console.log("valueh",value)
  }
  function handleRegister(value) {
    console.log("valueh",value)
  }

  function handleForgotPass(value) {
    console.log("valueh",value)
  }

  return (
    <ThemeProvider theme={theme}>
    <Router>
      <div>
        <nav>
          <ul className={classes.menuList}>
            <li className={classes.item}>
              <NavLink to="/" activeClassName="hurray">
                {/* React */}
              </NavLink>
            </li>
            {/* <li className={classes.item}>
              <Link to="/Register">Register</Link>
            </li>
            <li className={classes.item}>
              <Link to="/ForgotPass">ForgotPass</Link>
            </li> */}
          </ul>
        </nav>

        <Switch>
          <Route path="/Register">
          <MultiLogin Register="register" handleEvent={handleRegister}/>
          </Route>
          <Route path="/ForgotPass">
          <MultiLogin ForgotPass="forgotPass" handleEvent={handleForgotPass} />
          </Route>
          <Route path="/">
            <MultiLogin Login="login" handleEvent={handleLogin} />
          </Route>
        </Switch>
      </div>
    </Router>
    </ThemeProvider>
  );
}


export default withStyles(styles)(App);
